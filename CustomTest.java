package com.outfit7.test;

import io.appium.java_client.MobileElement;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Customtest extends BaseTest {

    @Test
    void filmVideo() throws InterruptedException{
        //Start recording
        driver.findElement(By.id("com.outfit7.talkingtom:id/recorderButton")).click();
        //Punch him three times in stomach
        for(int i=0;i<3;i++) {
            driver.findElement(By.xpath("//android.widget.RelativeLayout/android.view.View[@index='4']")).click();
            Thread.sleep(2000);
        }
        Thread.sleep(1500);
        //Tom farts
        driver.findElement(By.id("com.outfit7.talkingtom:id/gasmask")).click();
        Thread.sleep(2000);
        //Stop recording
        driver.findElement(By.id("com.outfit7.talkingtom:id/recorderButton")).click();
        //Save video
        driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
        //Enable permission
        MobileElement allowPermissionButton = driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button"));
        if (allowPermissionButton != null) {
            allowPermissionButton.click();
        }
        //Close window
        Thread.sleep(3000);
        MobileElement closeRecording = driver.findElement(By.id("com.outfit7.talkingtom:id/recorderMenuButtonClose"));
        closeRecording.click();
        closeRecording.click();
        System.out.println("###We have saved the video###");

    }
}
