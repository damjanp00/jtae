# Junior Test Automation Engineer Expertise Test
**Damjan Pjević, 06. 08. 2020**

Hi. Thank you for taking the time to review my application and this Expertise Test. 
For each task, I wrote a separate *.java* file. In this documentation, each task has a description and a snippet from the main code. In each *.java* file I have included extra comments. For any further explanation or inconsistencies please feel free to contact me.
To keep it simple, I tested by simply running the code without using TestNG or Extent Reports Framework. I have used Appium and UiAutomator Viewer tool from SDK to find all the elements.
I have also included a *sampleTest.zip* that icludes the whole solution.



### Task 1
**(BaseTest.java)**
> *Test needs to get to main scene where Tom resides. (This task is already solved as an example, so you get the feel how to navigate through the app)*

The following task has already been solved. However, I made two slight adjustments.
If both the client and the server are connected to the same network, we can establish a connection without the need for a cable. We can use the *udid* capability to find the client.
```java
capabilities.setCapability("udid","88.200.98.75:5555");
```

I had one issue. The provided code did not open the main scene on my device. The automation got stuck on the *permission window*. I had to change the following code:
from: `(By.id("com.android.packageinstaller:id/permission_allow_button"))`
to: `(By.id("com.android.permissioncontroller:id/permission_allow_button"))`
Final solution:
```java
//Sometimes we get asked for special permission
MobileElement allowPermissionButton = driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button"));
if (allowPermissionButton != null) {
    allowPermissionButton.click();
}
```

### Task 2
**(InfoTest.java)**
> *Go to info, press how to play and assert that first paragraph is correct “Talk to Tom and he repeats with a funny voice”*

After the BaseTest.java gets executed we can proceed with checking if the first paragraph is correct. First, the automation had to click on the *Info Button* and afterwards on *How to Play*. Game rules appeared on the screen. Since we are only interested in the first item we can find it by using the className. For a more universal approach, we could find the element by xpath.
(`driver.findElement(By.xpath("//android.view.View[@index='0']"));`)
If we wanted for example to check the second paragraph, we would have to use the xpath and change the index to 1.

```java
@Test
    void checkInfo() {
        driver.findElement(By.id("com.outfit7.talkingtom:id/buttonInfo")).click();
        driver.findElement(By.id("com.outfit7.talkingtom:id/infoWebButtonHowToPlay")).click();
        
        MobileElement result= driver.findElement(By.className("android.view.View"));
        String res = result.getText();
        
        if(res.equals("Talk to Tom and he repeats with a funny voice")) {
            System.out.println("Correct");
        }
    }
```

### Task 3
**(FoodTest.java)**
> *Feed him with some food (Button in the bottom right corner), make sure he is indeed eating something and take screenshot*

I gave Tom some cake using the code below. The only thing I had to note was the animation duration after the *Food button* is pressed.
You can check out the screenshot of him eating the cake on the following link: https://i.imgur.com/nx8semS.png

```java
    @Test
    void clickOnFoodButton() throws InterruptedException {
        driver.findElement(By.id("com.outfit7.talkingtom:id/foodButton")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("com.outfit7.talkingtom:id/foodItemCake")).click();
    }
```
### Task 4
**(CustomTest.java)**
> *Create a custom test scenario and write documentation for it. Place documentation in README.md*

For the custom test, I have recorded a video and saved it to internal storage.
The first task was to find the record button and click it. When the button is pressed I wanted Tom to do something. I came up with a scenario where the automation punched him three times in the stomach. Afterwards, Tom farts. To end the test the automation had to once again find the record button and click it. When the save window pop-ups the button “Gallery” had to be found. To finish, the goal was to come back to the main screen.
I have attached the recorded video in the provided folder. You can also view it on the following link: https://imgur.com/a/t8QlJYx

```java
    @Test
    void filmVideo() throws InterruptedException{
        driver.findElement(By.id("com.outfit7.talkingtom:id/recorderButton")).click();
        
        for(int i=0;i<3;i++) {
            driver.findElement(By.xpath("//android.widget.RelativeLayout/android.view.View[@index='4']")).click();
            Thread.sleep(2000);
        }
        Thread.sleep(1500);
        driver.findElement(By.id("com.outfit7.talkingtom:id/gasmask")).click();
        Thread.sleep(2000);
        
        driver.findElement(By.id("com.outfit7.talkingtom:id/recorderButton")).click();
        driver.findElement(By.xpath("//android.widget.TextView[@text='Gallery']")).click();
        MobileElement allowPermissionButton = driver.findElement(By.id("com.android.permissioncontroller:id/permission_allow_button"));
        if (allowPermissionButton != null) {
            allowPermissionButton.click();
        }
        
        Thread.sleep(3000);
        MobileElement closeRecording = driver.findElement(By.id("com.outfit7.talkingtom:id/recorderMenuButtonClose"));
        closeRecording.click();
        closeRecording.click();

    }
```