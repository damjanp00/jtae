package com.outfit7.test;

import io.appium.java_client.MobileElement;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InfoTest extends BaseTest {
    private String text="Talk to Tom and he repeats with a funny voice";
    @Test
    void checkInfo() {
        //Open "How to Play"
        driver.findElement(By.id("com.outfit7.talkingtom:id/buttonInfo")).click();
        driver.findElement(By.id("com.outfit7.talkingtom:id/infoWebButtonHowToPlay")).click();

        //Find the first paragraph
        MobileElement result= driver.findElement(By.className("android.view.View"));
        /*For a more universal approach we should use the following line instead*/
        //MobileElementresult = driver.findElement(By.xpath("//android.view.View[@index='0']"));

        //Check if the paragraph is correct
        String res = result.getText();
        if(res.equals(text)) {
            System.out.println("Correct");
        }
        /*Print out the result:*/
        //System.out.println(res);

    }
}
